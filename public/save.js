const scoreKey = "score";
const localKey = "p10-yahtzee-phases";
const checkboxes = document.querySelectorAll("input[type=checkbox]");

checkboxes.forEach((checkbox) => {
    checkbox.addEventListener("change", () => {
        const phases = Array.from(checkboxes).filter(i => i.checked).map(i => i.id);
        window.localStorage.setItem(localKey, phases.join(","));

        const score = phases.reduce((sum, id) => sum + parseInt(id.slice(1)), 0);	
        document.getElementById(scoreKey).innerHTML = `Your Score: ${score}`;
    })
});

let previousScore = 0;
const previousPhases = window.localStorage.getItem(localKey);
if (previousPhases !== null) {
    previousPhases.split(",").forEach((id) => {
        document.getElementById(id).checked = true;
        previousScore += parseInt(id.slice(1));
    });
    document.getElementById(scoreKey).innerHTML = `Your Score: ${previousScore}`;
}
